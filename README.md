#taste-msui(仿味觉大师app的webapp)

## 说明 
此项目用于技术交流和学习，以味觉大师这款app为原型，设定实现目标，在此向 味觉大师app 致敬

另外 项目包含一款以此项目为基础、使用appcan以及apicloud制作的两个app，欢迎体验
###使用技术
* jfinal
* sui mobile
* vuejs
* zepto


### 部署环境前提
* MyEclipse(或eclipse)
* jdk7
* git
* tomcat
* mysql

### 项目配置

下载地址: http://git.oschina.net/xiongmao1114/taste-msui.git

导入到MyEclipse中的具体配置同一般web项目.

项目使用jetty作web容器

mysql数据文件为taste.sql

mysql连接配置文件为res/a_litte_config.txt

启动入口为src/zero/MyConfig.java(此文件中包含main函数),以java application方式启动即可

###相关技术参考

[jfinal 官网 ](http://www.jfinal.com/)

[vuejs ](http://cn.vuejs.org/guide/)

[sui mobile ](http://m.sui.taobao.org/)

### 联系我
* email: xiongmao1114@163.com

### app截图
![image](http://7xiakq.com1.z0.glb.clouddn.com/taste/images/index.png)

![image](http://7xiakq.com1.z0.glb.clouddn.com/taste/images/show.png)






