package zero;


import pluto.controller.MasterController;
import pluto.controller.NewsController;
import pluto.controller.RestaurantController;
import pluto.controller.ShowController;
import pluto.controller.index.IndexController;
import pluto.controller.index.MasterOfIndexController;
import pluto.controller.index.MyOfIndexController;
import pluto.controller.index.RestaurantOfIndexController;
import pluto.controller.index.ShowOfIndexController;
import pluto.handler.RequestURLHandler;
import pluto.model._MappingKit;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.interceptor.Restful;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.SqlReporter;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;


/**
 * API引导式配置
 */
public class MyConfig extends JFinalConfig {

	@Override
	public void afterJFinalStart() {
		// TODO Auto-generated method stub
		super.afterJFinalStart();

	}

	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		// 加载少量必要配置，随后可用getProperty(...)获取值
		loadPropertyFile("a_little_config.txt");
		me.setDevMode(getPropertyToBoolean("devMode", false));
		//me.setError500View("/WEB-INF/pages/transition/invalid.html");
	}

	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		
		me.add("/", IndexController.class);
		me.add("/showOfIndex", ShowOfIndexController.class);
		me.add("/masterOfIndex", MasterOfIndexController.class);
		me.add("/restaurantOfIndex", RestaurantOfIndexController.class);
		me.add("/myOfIndex", MyOfIndexController.class);
		me.add("/show", ShowController.class);
		me.add("/news", NewsController.class);
		me.add("/master", MasterController.class);
		me.add("/restaurant", RestaurantController.class);
		
		
	}

	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		// 配置C3p0数据库连接池插件
		// C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbcUrl"),
		// getProperty("user"),
		// getProperty("password").trim(),getProperty("driver"));
		// me.add(c3p0Plugin);
		DruidPlugin druidPlugin = new DruidPlugin(getProperty("jdbcUrl"),
				getProperty("user"), getProperty("password").trim(),
				getProperty("driver"));
//		druidPlugin.setMaxActive(300);
		me.add(druidPlugin);
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		arp.setDialect(new MysqlDialect());
		arp.setShowSql(true);
		me.add(arp);

		SqlReporter.setLog(true);
		
		_MappingKit.mapping(arp);
	}

	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		me.addGlobalActionInterceptor(new Restful());
	}

	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		me.add(new RequestURLHandler());
	}

	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目 运行此 main
	 * 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	 public static void main(String[] args) {
	 JFinal.start("WebRoot", 8080, "/", 5);
	 
	 
	 }
}
