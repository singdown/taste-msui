package pluto.controller.base;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import pluto.util.ImageToBase64;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Model;

public class BaseController extends Controller {
	private static final String basePath = "/WEB-INF/pages";

	public void myRender(String view) {
		render(basePath + view);
	}

	protected List convertImg(List models) {
		return convertImg(models, "img_path");
	}
	protected List convertImg(List models, String imgField) {
		String contextPath = getRequest().getServletContext().getRealPath("/");
		for (int i=0;i<models.size();i++) {
			Model model=(Model) models.get(i);
			model.set(
					imgField,
					ImageToBase64.getImageBase64(contextPath
							+ model.getStr(imgField)));
		}
		return models;
	}
}
