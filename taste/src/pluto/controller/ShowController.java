package pluto.controller;

import pluto.controller.base.BaseController;
import pluto.controller.dto.MyAppJSON;
import pluto.model.News;
import pluto.model.Show;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.Restful;
public class ShowController extends BaseController {
	public void index(){
		myRender("/show.html");
	}
	public void show(){
//		setAttr("one", Show.dao.findById(getParaToInt()));
//		myRender("/show/detail.html");
		MyAppJSON<Show> appJSON=new MyAppJSON<Show>();
		appJSON.addItem(Show.dao.findById(getParaToInt()));
		renderJson(appJSON);
	}
}
