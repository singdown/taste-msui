package pluto.controller.index;

import pluto.controller.base.BaseController;
import pluto.controller.dto.MyAppJSON;
import pluto.model.Master;
import pluto.model.MasterMaterial;
import pluto.model.MasterMethod;
import pluto.model.MasterTip;
import pluto.model.Show;

public class MasterOfIndexController extends BaseController {
	public void index(){
	}
	
	public void initJSON(){
		MyAppJSON<Master> appJSON=new MyAppJSON();
		appJSON.addItems(Master.dao.findForIndex(10).getList());
		renderJson(appJSON);
	}
}
