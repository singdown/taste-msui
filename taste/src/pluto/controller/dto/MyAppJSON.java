package pluto.controller.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Record;

public class MyAppJSON<T> {
	public static final String ERR_LOGIN="10";
	
	private String errCode="0";
	private String message;
	private List<T> items;
	private Map<String, List<T>> itemMaps;
	public String getErrCode() {
		return errCode;
	}
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<T> getItems() {
		return items;
	}
	public void addItem(T item) {
		if (items==null) {
			items=new ArrayList<T>();
		}
		items.add(item);
	}
	public void addItems(List<T> items) {
		if (this.items==null) {
			this.items=new ArrayList<T>();
		}
		this.items.addAll(items);
	}
	public Map<String, List<T>> getItemMaps() {
		return itemMaps;
	}
	public void addItemMap(String key, List<T> items) {
		if (this.itemMaps==null) {
			this.itemMaps=new HashMap<String, List<T>>();
		}
		this.itemMaps.put(key, items);
	}
	
	
	
}
